#include <Engine.h>

int main(int argc, char argv) {
    Engine e; // Create a pointer to a engine
    return e.run(); // run the engine
}