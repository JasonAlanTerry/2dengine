//
// Created by jasonterry on 1/18/17.
//

#include "Engine.h"

// Responsibility for Engine
// Setup SDL
// Setup the Active Window

// -- Constructors and Destructors

Engine::Engine() {
    // Setup data here... Init ENGINE stuff only, offload
    // non engine tech to init
    // TODO-- Build Screen Size here??
    running = true;

}

Engine::~Engine() {


}

// -- Private Methods --

// Returns 0 on success, -1 on error.
int Engine::init() {
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        // Error
        return -1;
    } else {
        eWindow = SDL_CreateWindow("2dEngine", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
        if (eWindow == NULL) {
            // Error
            return -1;
        } else {
            winSurface = SDL_GetWindowSurface( eWindow );
        }
    }
    return 0;
}

// Returns 0 on success, -1 on error.
int Engine::loop() {

    while (running) {
    SDL_FillRect( winSurface, NULL, SDL_MapRGB( winSurface->format, 0xFF, 0xFF, 0xFF ) ); //Update the surface
    SDL_UpdateWindowSurface( eWindow ); //Wait two seconds
    }

    return 0;
}

// -- Public Methods --

// Returns 0 on success, -1 on error.
int Engine::run() {

    if (init() < 0) {
        // Error
        return -1;
    }

    return this->loop();
}