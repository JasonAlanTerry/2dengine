//
// Created by jasonterry on 1/18/17.
//

#ifndef INC_2DENGINE_ENGINE_H
#define INC_2DENGINE_ENGINE_H

#include "SDL2/SDL.h"

class Engine {

private:

    // Temp Constant/Fixed screen size
    // Plan to replace with variable setup / full screen.
    const int SCREEN_WIDTH = 640;
    const int SCREEN_HEIGHT = 480;

    bool running;

    SDL_Window * eWindow = NULL;
    SDL_Surface * winSurface = NULL;

    int init();

    int loop();

public:

    Engine();

    ~Engine();

    int run();

};


#endif //INC_2DENGINE_ENGINE_H
